import * as React from "react";
import * as ReactDOM from "react-dom";
import "CSS/index.scss";

ReactDOM.render(
  <div>
    <p>Hello World!</p>
  </div>,
  document.getElementById("root") as HTMLElement
);
