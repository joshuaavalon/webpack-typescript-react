const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

const resolvePath = relativePath => {
  return path.resolve(__dirname, relativePath);
};

module.exports = (env, args) => {
  const isProduction = args.mode === "production";
  return {
    mode: args.mode,
    entry: {
      app: ["@babel/polyfill", resolvePath("src/index.tsx")]
    },
    devtool: isProduction ? "source-map" : "eval",
    module: {
      rules: [
        {
          test: /\.(tsx?|jsx?)$/,
          include: /src/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },
        {
          test: /\.js$/,
          use: "source-map-loader",
          enforce: "pre"
        },
        {
          test: /\.html$/,
          use: [{ loader: "html-loader", options: { minimize: true } }]
        },
        {
          test: /\.(png|jpg|gif|woff(2)?|eot|ttf|svg)$/,
          use: [
            {
              loader: "url-loader",
              options: {
                limit: 8192,
                fallback: "file-loader",
                name: "[path][name].[ext]",
                context: "src"
              }
            }
          ]
        },
        {
          test: /\.scss$/,
          use: [
            isProduction ? MiniCssExtractPlugin.loader : "style-loader",
            "css-loader",
            "sass-loader"
          ]
        }
      ]
    },
    output: {
      filename: isProduction ? "[name].[contenthash].js" : "[name].[hash].js"
    },
    plugins: [
      new CleanWebpackPlugin([resolvePath("dist")]),
      new HtmlWebPackPlugin({
        template: resolvePath("src/index.html"),
        filename: "index.html"
      }),
      new MiniCssExtractPlugin({
        filename: "[name].[contenthash].css",
        chunkFilename: "[id].css"
      })
    ],
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
      alias: {
        "@": resolvePath("src/"),
        CSS: resolvePath("src/css/"),
        Image: resolvePath("src/img/"),
        Font: resolvePath("src/font/"),
      }
    },
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          parallel: true,
          sourceMap: true,
          uglifyOptions: {
            compress: args["optimize-minimize"],
            output: { comments: false }
          }
        }),
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            map: {
              inline: false
            }
          }
        })
      ]
    }
  };
};
